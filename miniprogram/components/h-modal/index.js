Component({
  externalClasses: ['i-class', 'i-class-mask'],

  properties: {
    visible: {
      type: Boolean,
      value: false
    },
    title: {
      type: String,
      value: ''
    },
    isAutoVisible: {
      type: Boolean,
      value: false
    }
  },
  methods: {
    navigateTo(event) {

      if (!this.data.isAutoVisible)
        return;

      this.setData({
        visible: false
      });
    }
  }
});